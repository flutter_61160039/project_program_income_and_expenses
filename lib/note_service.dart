import 'package:cloud_firestore/cloud_firestore.dart';

CollectionReference notes = FirebaseFirestore.instance.collection('notes');
Future<void> checkNote(userId) {
  //print(userId);
  return notes.where('uId', isEqualTo: userId).get().then((value) {
    if (value.size < 1) {
      addNote(userId);
    }
  }).catchError((error) => print('Failed to get note: $error'));

}

Future<void> addWalletNote(userId,uId,note) {
  
  return notes.where('uId', isEqualTo: userId).get().then((value) {
    if (value.size < 1) {
      updateNote(userId,uId,note) ;
    }
  }).catchError((error) => print('Failed to get note: $error'));
  
}

Future<void> addNote(uId) {
  return notes
      .add({
        'uId': uId,
        'noteData': '',
      })
      .then((value) => print('notes Added '))
      .catchError((error) => print('Failed to add notes: $error'));
}

Future<void> updateNote(noteId,uId,note) {
  return notes
      .doc(noteId)
      .update({
        'uId': uId,
        'noteData': note,
      })
      .then((value) => print('note Updateed'))
      .catchError((error) => print('Failed to update note: $error'));
}
