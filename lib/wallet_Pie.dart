import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:pie_chart/pie_chart.dart';
import 'package:flutter/material.dart';

class pieChartWallet extends StatefulWidget {
  pieChartWallet({Key? key}) : super(key: key);

  @override
  _pieChartWalletState createState() => _pieChartWalletState();
}

class _pieChartWalletState extends State<pieChartWallet> {
  double income = 0;
  double expense = 0;
  String? _imageURL = null;
  String? _nameUser = null;
  @override
  void initState() {
    super.initState();
    setState(() {
      _imageURL = FirebaseAuth.instance.currentUser!.photoURL;
      _nameUser = FirebaseAuth.instance.currentUser!.displayName;
    });
  }

  final Stream<QuerySnapshot> _walletListskStream = FirebaseFirestore.instance
      .collection('wallet')
      .where('uId', isEqualTo: FirebaseAuth.instance.currentUser!.uid)
      .snapshots();

  Map<String, double> _loadWelletMap() {
    Map<String, double> dataMap = {
      'รายรับ': income,
      'รายจ่าย': expense,
    };
    return dataMap;
  }

  List<Color> colorlist = [Colors.blue, Colors.red];

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: _walletListskStream,
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
        if (snapshot.hasError) {
          return Text('Something went wrong');
        }
        if (snapshot.connectionState == ConnectionState.waiting) {
          return Text('Loading');
        }
        income = 0;
        expense = 0;
        snapshot.data!.docs.map((DocumentSnapshot document) {
          Map<String, dynamic> data = document.data()! as Map<String, dynamic>;
          if (data['type'] == '1') {
            income += data['money'];
          } else if (data['type'] == '2') {
            expense += data['money'];
          }
        }).toList();

        return ListView(
          children: [
            Column(
              children: [
                Card(
                  //clipBehavior: Clip.antiAlias,
                  child: Column(
                    children: [
                      Text(
                        'รายรับรายจ่ายของ\n$_nameUser',
                        style: TextStyle(
                            fontSize: 40,
                            color: Colors.black,
                            fontWeight: FontWeight.normal),
                      ),
                      PieChart(
                        dataMap: _loadWelletMap(),
                        chartLegendSpacing: 10,
                        colorList: colorlist,
                        chartValuesOptions: ChartValuesOptions(
                          showChartValuesInPercentage: true,
                        ),
                        chartRadius: MediaQuery.of(context).size.width / 3.5,
                      ),
                      Text('สรุปยอดเงิน $income-$expense',
                          style: TextStyle(
                              fontSize: 20, fontWeight: FontWeight.normal)),
                      Text((income - expense).toString(),
                          style: TextStyle(
                              fontSize: 40,
                              color: (income - expense) >= 0
                                  ? Colors.green
                                  : Colors.red,
                              fontWeight: FontWeight.normal))
                    ],
                  ),
                )
              ],
            )
          ],
        );
      },
    );
  }
}
