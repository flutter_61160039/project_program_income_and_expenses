import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_authen/notePage.dart';
import 'package:flutter_authen/note_service.dart';
//import 'package:flutter_authen/note_service.dart';
import 'package:flutter_authen/wallet_form.dart';
import 'package:flutter_authen/wallet_Pie.dart';

import 'wallet_service.dart';

class UserInformation extends StatefulWidget {
  UserInformation({Key? key}) : super(key: key);

  @override
  _UserInformationState createState() => _UserInformationState();
}

class _UserInformationState extends State<UserInformation> {
  String addNote = '';
  String uId = FirebaseAuth.instance.currentUser!.uid;
  CollectionReference notes = FirebaseFirestore.instance.collection('notes');
  final Stream<QuerySnapshot> _userStream = FirebaseFirestore.instance
      .collection('wallet')
      .where('uId', isEqualTo: FirebaseAuth.instance.currentUser!.uid)
      //.orderBy('selectedDate' ,descending: true)
      .snapshots();

  Future<void> _showDialog(id) {
    Widget cancelButton = TextButton(
      child: new Text("ยกเลิก"),
      onPressed: () {
        Navigator.of(context).pop();
      },
    );
    Widget continueButton = TextButton(
      child: Text(
        "ตกลง",
        style: TextStyle(color: Colors.red),
      ),
      onPressed: () {
        Navigator.of(context).pop();
        setState(() {
          delUser(id);
        });
      },
    );
    return showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text("ลบรายการ"),
          content: Text("คุณต้องการลบรายการ ?"),
          actions: <Widget>[continueButton, cancelButton],
        );
      },
    );
  }

  void tapUpdate(id) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => UserForm(
                  userId: id,
                )));
  }

  void addToNote(date, type, sort, money) {
    addNote += '$date \n$sort\n$type  $money\n';
    print(addNote);
    _loadNoteData();
  }

  Future<void> _loadNoteData() {
    return notes.where('uId', isEqualTo: uId).get().then((value) {
      value.docs.forEach((data) {
        String userId = data.id;
        String noteData = data['noteData'];
        noteData += '$addNote';
        addWalletNote(userId, uId, noteData);
      });
    }).catchError((error) => print('Failed to Load note: $error'));
  }

  List<ListTile> _createListTie(AsyncSnapshot<QuerySnapshot> snapshot) {
    List<ListTile> newColumn =
        snapshot.data!.docs.map((DocumentSnapshot document) {
      Map<String, dynamic> data = document.data()! as Map<String, dynamic>;
      return ListTile(
        leading: IconButton(
            icon: Icon(Icons.note_add_rounded),
            onPressed: () {
              addToNote(
                  data['selectedDate'].toDate().toString(),
                  data['type'] == '1' ? 'รายรับ' : 'รายจ่าย',
                  data['sort'],
                  data['money'].toString());
              final snackBar = SnackBar(
                backgroundColor: Colors.green,
                content: const Text('เพิ่มข้อมูลไปยังโน้ต!'),
              );
              ScaffoldMessenger.of(context).showSnackBar(snackBar);
            }), //
        title: Row(
          children: [
            Text(data['sort']),
            Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(
                    data['type'] == '1' ? '+' : '-',
                    style: TextStyle(color: Colors.black),
                  ),
                  Text(
                    data['money'].toString(),
                    style: TextStyle(
                        color: data['type'] == '2' ? Colors.red : Colors.green),
                  )
                ],
              ),
            )
          ],
        ),
        subtitle: Text(data['selectedDate'].toDate().toString()),
        trailing: IconButton(
          icon: Icon(Icons.delete),
          onPressed: () async {
            await _showDialog(document.id); //
          },
        ),
        onTap: () {
          setState(() {
            tapUpdate(document.id);
          });
        },
      );
    }).toList();


    return newColumn;
  }

  String sort = '';
  String? _imageURL = null;
  String? _nameUser = null;
  @override
  void initState() {
    super.initState();
    setState(() {
      _imageURL = FirebaseAuth.instance.currentUser!.photoURL;
      _nameUser = FirebaseAuth.instance.currentUser!.displayName;
    });
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
        stream: _userStream,
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.hasError) {
            return Text('Something went wrong');
          }
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Text('Loading..');
          }
          return snapshot.data!.size > 0? ListView(children: _createListTie(snapshot)):Center(child: Text('ไม่มีรายการ'),);
        });
  }
}
