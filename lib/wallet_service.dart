import 'package:cloud_firestore/cloud_firestore.dart';

CollectionReference users = FirebaseFirestore.instance.collection('wallet');
Future<void> delUser(userId) {
    return users
        .doc(userId)
        .delete()
        .then((value) => print('Wallet Delete'))
        .catchError((error) => print('Failed to delete wallet: $error'));
  }

  Future<void> addWallet(uId,selectedDate,type,sort,money) {
    return users
        .add({
          'uId': uId,
          'selectedDate': selectedDate,
          'type': type,
          'sort': sort,
          'money': int.parse(money)
        })
        .then((value) => print('Wallet Added '))
        .catchError((error) => print('Failed to add wallet: $error'));
  }

  Future<void> updateWallet(userId,uId,selectedDate,type,sort,age) {
    return users
        .doc(userId)
        .update({
          'uId': uId,
          'selectedDate': selectedDate,
          'type': type,
          'sort': sort,
          'money': int.parse(age)
        })
        .then((value) => print('Wallet Updateed'))
        .catchError((error) => print('Failed to update wallet: $error'));
  }