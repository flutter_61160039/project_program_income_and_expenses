import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_authen/wallet_Pie.dart';

class DrawerWidget extends StatefulWidget {
  Color color;
  DrawerWidget({Key? key, required this.color}) : super(key: key);

  @override
  _DrawerWidgetState createState() => _DrawerWidgetState(this.color);
}

class _DrawerWidgetState extends State<DrawerWidget> {
  _DrawerWidgetState(this.color);
  Color color;
  String? _imageURL = null;
  String? _nameURL = null;
  double income = 0;
  double expense = 0;
  final Stream<QuerySnapshot> _walletListskStream = FirebaseFirestore.instance
      .collection('wallet')
      .where('uId', isEqualTo: FirebaseAuth.instance.currentUser!.uid)
      .snapshots();

  @override
  void initState() {
    super.initState();
    setState(() {
      _imageURL = FirebaseAuth.instance.currentUser!.photoURL;
      _nameURL = FirebaseAuth.instance.currentUser!.displayName;
    });
  }

  double sumMoney(snapshot) {
    income = 0;
    expense = 0;
    snapshot.data!.docs.map((DocumentSnapshot document) {
      Map<String, dynamic> data = document.data()! as Map<String, dynamic>;
      if (data['type'] == '1') {
        income += data['money'];
      } else if (data['type'] == '2') {
        expense += data['money'];
      }
    }).toList();
    return (income - expense);
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
        stream: _walletListskStream,
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.hasError) {
            return Text('Something went wrong');
          }
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Text('Loading..');
          }
          return Drawer(
            
            child: ListView(
              children: [
                DrawerHeader(
                    decoration: BoxDecoration(color: color),
                    child: Center(
                      child: ClipOval(
                        child: Image.network(
                          _imageURL!,
                          width: 100,
                          height: 100,
                          fit: BoxFit.fill,
                        ),
                      ),
                    )),
                Container(
                  padding: EdgeInsets.all(16),
                  child: Column(
                    children: [
                      Text(
                        _nameURL!,
                        style: TextStyle(fontSize: 20),
                      ),
                      Text(
                        'สรุปยอดเงิน',
                        style: TextStyle(fontSize: 20),
                      ),
                      Text(sumMoney(snapshot).toString(),
                          style: TextStyle(
                            fontSize: 20,
                            color: (income - expense) >= 0
                                ? Colors.green
                                : Colors.red,
                          ))
                    ],
                  ),
                )
              ],
            ),
          );
        });
  }
}
