import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

import 'note_service.dart';

class NoteForm extends StatefulWidget {
  NoteForm({Key? key}) : super(key: key);

  @override
  _NoteFormState createState() => _NoteFormState();
}

class _NoteFormState extends State<NoteForm> {
  String? _imageURL = null;
  String? _nameUser = null;
  String userId = '';
  String noteData = " ";
  String uId = FirebaseAuth.instance.currentUser!.uid;

  CollectionReference users = FirebaseFirestore.instance.collection('wallet');
  CollectionReference notes = FirebaseFirestore.instance.collection('notes');
  TextEditingController _noteController = new TextEditingController();

  @override
  void initState() {
    super.initState();
    _imageURL = FirebaseAuth.instance.currentUser!.photoURL;
    _nameUser = FirebaseAuth.instance.currentUser!.displayName;

    setState(() {
      _loadData();
    });
  }

  Future<void> _loadData() {
    return notes.where('uId', isEqualTo: uId).get().then((value) {
      value.docs.forEach((data) {
        userId = data.id;
        noteData = data['noteData'];
        _noteController.text = noteData;
      });
    }).catchError((error) => print('Failed to get note: $error'));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('โน้ต'),
          centerTitle: true,
          actions: <Widget>[
            Row(
              children: [
                _imageURL == null
                    ? const Icon(Icons.person)
                    : GestureDetector(
                        child: ClipOval(
                          child: Image.network(
                            _imageURL!,
                            width: 40,
                            height: 40,
                            fit: BoxFit.fill,
                          ),
                        ),
                      )
              ],
            ),
            Center(
              child: Container(
                padding: EdgeInsets.all(10),
                child: Text(
                  _nameUser.toString(),
                  style: TextStyle(color: Colors.white),
                ),
              ),
            )
          ],
        ),
        body: ListView(
          children: [
            Column(
              children: [
                Container(
                  padding: EdgeInsets.all(16.0),
                  child: Form(
                      child: Column(
                    children: [
                      ListTile(
                        title: Text('โน้ต', style: TextStyle(fontSize: 20)),
                      ),
                      TextFormField(
                        autofocus: true,
                        controller: _noteController,
                        keyboardType: TextInputType.multiline,
                        maxLines: 18,
                        decoration: InputDecoration(
                            hintText: 'เพิ่มข้อความ',
                            border: OutlineInputBorder()),
                        onChanged: (value) {
                          setState(() {
                            noteData = value;
                          });
                        },
                      ),
                      SizedBox(
                        child: Text(''),
                      ),
                    ],
                  )),
                ),
              ],
            ),
            Container(
              padding: EdgeInsets.all(16),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Expanded(
                    child: Container(),
                  ),
                  SizedBox(
                    width: 200,
                    height: 50,
                    child: ElevatedButton(
                      
                      onPressed: () async {
                        await updateNote(userId, uId, noteData);

                        Navigator.pop(context);
                      },
                      child: Text('บันทึก'),
                      style:
                          ElevatedButton.styleFrom(padding: EdgeInsets.all(16)),
                    ),
                  ),
                  Expanded(
                    child: Container(),
                  ),
                  SizedBox(
                    //width: 200,
                    height: 50,
                    child: ElevatedButton(
                      onPressed: () async {
                        // await updateNote(userId, uId, noteData);

                        Navigator.pop(context);
                      },
                      child: Text('ยกเลิก'),
                      style:
                          ElevatedButton.styleFrom(padding: EdgeInsets.all(16)),
                    ),
                  ),
                  Expanded(
                    child: Container(),
                  ),
                ],
              ),
            )
          ],
        ));
  }
}
