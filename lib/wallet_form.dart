import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';

import 'wallet_service.dart';

class UserForm extends StatefulWidget {
  String userId;
  UserForm({Key? key, required this.userId}) : super(key: key);

  @override
  _UserFormState createState() => _UserFormState(this.userId);
}

class _UserFormState extends State<UserForm> {
  String? _imageURL = null;
  String? _nameUser = null;
  String textTime = 'กดเลือกวันที่และเวลา';
  String userId;
  String uId = FirebaseAuth.instance.currentUser!.uid;
  DateTime selectedDate = DateTime.now();
  String type = "1";
  String sort = "เงินเดือน";
  String money = "0";
  CollectionReference users = FirebaseFirestore.instance.collection('wallet');
  _UserFormState(this.userId);
  TextEditingController _moneyController = new TextEditingController();
  static List<Color> _colorLists = <Color>[
    Colors.blue,
    Colors.green,
  ];

  @override
  void initState() {
    super.initState();
    _imageURL = FirebaseAuth.instance.currentUser!.photoURL;
    _nameUser = FirebaseAuth.instance.currentUser!.displayName;
    if (this.userId.isNotEmpty) {
      users.doc(this.userId).get().then((snapshot) {
        if (snapshot.exists) {
          var data = snapshot.data() as Map<String, dynamic>;
          selectedDate = data['selectedDate'].toDate();
          textTime = this.selectedDate.toString();
          this.type = data['type'].toString();
          this.sort = data['sort'];
          money = data['money'].toString();
          _moneyController.text = money;
          setState(() {
            //print(data);
          });
        }
      });
    }
  }

  final _formkey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: _colorLists[this.userId.length < 5 ? 0 : 1],
          title: Text(this.userId.length < 5 ? 'เพิ่มรายการ' : 'แก้ไขรายการ'),
          centerTitle: true,
          actions: <Widget>[
            Row(
              children: [
                _imageURL == null
                    ? const Icon(Icons.person)
                    : GestureDetector(
                        child: ClipOval(
                          child: Image.network(
                            _imageURL!,
                            width: 40,
                            height: 40,
                            fit: BoxFit.fill,
                          ),
                        ),
                      )
              ],
            ),
            Center(
              child: Container(
                padding: EdgeInsets.all(10),
                child: Text(
                  _nameUser.toString(),
                  style: TextStyle(color: Colors.white),
                ),
              ),
            )
          ],
        ),
        body: ListView(
          children: [
            Column(
              children: [
                Container(
                  padding: EdgeInsets.all(16.0),
                  child: Form(
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      key: _formkey,
                      child: Column(
                        children: [
                          ListTile(
                            leading: Text(
                              'วันที่',
                              style: TextStyle(fontSize: 20),
                            ),
                            title: TextButton(
                              autofocus: true,
                              onPressed: () {
                                DatePicker.showDateTimePicker(
                                  context,
                                  showTitleActions: true,
                                  onChanged: (date) {
                                    print('change $date in time zone ' +
                                        date.timeZoneOffset.inHours.toString());
                                  },
                                  onConfirm: (date) {
                                    setState(() {
                                      selectedDate = date;
                                      textTime = selectedDate.toString();
                                      print('confirm $textTime');
                                    });
                                  },
                                  currentTime: selectedDate,
                                  locale: LocaleType.en,
                                );
                              },
                              child: Text(
                                textTime,
                                style: TextStyle(
                                    color: _colorLists[
                                        this.userId.length < 5 ? 0 : 1],
                                    fontSize: 20),
                              ),
                            ),
                          ),
                          ListTile(
                            title:
                                Text('รูปแบบ', style: TextStyle(fontSize: 20)),
                          ),
                          ListTile(
                            title: Text("รายรับ"),
                            leading: Radio(
                              value: 1,
                              groupValue: int.parse(type),
                              onChanged: (value) {
                                setState(() {
                                  type = value.toString();
                                });
                              },
                              activeColor:
                                  _colorLists[this.userId.length < 5 ? 0 : 1],
                            ),
                          ),
                          ListTile(
                            title: Text("รายจ่าย"),
                            leading: Radio(
                              value: 2,
                              groupValue: int.parse(type),
                              onChanged: (value) {
                                setState(() {
                                  type = value.toString();
                                });
                              },
                              activeColor:
                                  _colorLists[this.userId.length < 5 ? 0 : 1],
                            ),
                          ),
                          ListTile(
                            title: Text('หมวดหมู่',
                                style: TextStyle(fontSize: 20)),
                          ),
                          DropdownButtonFormField(
                            value: this.sort,
                            items: [
                              DropdownMenuItem(
                                child: Row(
                                  children: [
                                    Text('เงินเดือน'),
                                    SizedBox(width: 8.0)
                                  ],
                                ),
                                value: 'เงินเดือน',
                              ),
                              DropdownMenuItem(
                                child: Row(
                                  children: [
                                    Text('ค่าข้าว'),
                                    SizedBox(width: 8.0)
                                  ],
                                ),
                                value: 'ค่าข้าว',
                              ),
                              DropdownMenuItem(
                                child: Row(
                                  children: [
                                    Text('ค่าน้ำ'),
                                    SizedBox(width: 8.0)
                                  ],
                                ),
                                value: 'ค่าน้ำ',
                              ),
                              DropdownMenuItem(
                                child: Row(
                                  children: [
                                    Text('ค่าไฟ'),
                                    SizedBox(width: 8.0)
                                  ],
                                ),
                                value: 'ค่าไฟ',
                              ),
                            ],
                            onChanged: (String? newValue) {
                              setState(() {
                                sort = newValue!;
                              });
                            },
                          ),
                          ListTile(
                            title: Text('จำนวนเงิน',
                                style: TextStyle(fontSize: 20)),
                          ),
                          TextFormField(
                            controller: _moneyController,
                            decoration:
                                InputDecoration(labelText: 'จำนวน(บาท)'),
                            onChanged: (value) {
                              setState(() {
                                money = value;
                              });
                            },
                            validator: (value) {
                              if (value == null ||
                                  value.isEmpty ||
                                  int.tryParse(value) == null ||
                                  int.tryParse(value)! < 0) {
                                return 'กรุณา ใส่เงิน';
                              }
                              return null;
                            },
                          ),
                          SizedBox(
                            child: Text(''),
                          ),
                          Container(
                            padding: EdgeInsets.all(16),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Expanded(
                                  child: Container(),
                                ),
                                SizedBox(
                                  width: 200,
                                  height: 50,
                                  child: ElevatedButton(
                                    onPressed: () async {
                                      if (_formkey.currentState!.validate()) {
                                        if (userId.length < 5) {
                                          await addWallet(uId, selectedDate,
                                              type, sort, money);
                                        } else {
                                          await updateWallet(userId, uId,
                                              selectedDate, type, sort, money);
                                        }

                                        Navigator.pop(context);
                                      }
                                    },
                                    child: Text('บันทึก'),
                                    style: ElevatedButton.styleFrom(
                                        primary: _colorLists[
                                            this.userId.length < 5 ? 0 : 1],
                                        padding: EdgeInsets.all(16)),
                                  ),
                                ),
                                Expanded(
                                  child: Container(),
                                ),
                                SizedBox(
                                  //width: 200,
                                  height: 50,
                                  child: ElevatedButton(
                                    onPressed: () async {
                                      // await updateNote(userId, uId, noteData);

                                      Navigator.pop(context);
                                    },
                                    child: Text('ยกเลิก'),
                                    style: ElevatedButton.styleFrom(
                                        primary: _colorLists[
                                            this.userId.length < 5 ? 0 : 1],
                                        padding: EdgeInsets.all(16)),
                                  ),
                                ),
                                Expanded(
                                  child: Container(),
                                ),
                              ],
                            ),
                          )
                        ],
                      )),
                ),
              ],
            )
          ],
        ));
  }
}
