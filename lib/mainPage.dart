import 'dart:io';
import 'package:file_picker/file_picker.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_authen/drawer_widget.dart';
import 'package:flutter_authen/homePage.dart';
//import 'package:flutter_authen/notePage.dart';
import 'package:flutter_authen/wallet_Pie.dart';
import 'package:flutter_authen/wallet_information.dart';
import 'package:firebase_storage/firebase_storage.dart' as firebase_storage;
import 'package:flutter_authen/wallet_service.dart';
import 'note_service.dart';
import 'wallet_form.dart';
import 'package:flutter/cupertino.dart';
import 'dart:async';

class mainPage extends StatefulWidget {
  mainPage({Key? key}) : super(key: key);
  @override
  _mainPageState createState() => _mainPageState();
}

class _mainPageState extends State<mainPage> {
  String uId = FirebaseAuth.instance.currentUser!.uid;
  String? _imageURL = null;
  int _pageIndex = 0;
  List _appBarLists = [
    'หน้าหลัก',
    'จัดการรายการ',
    'สรุปรายการ',
  ];
  static List<StatefulWidget> _widgetLists = <StatefulWidget>[
    HomePage(),
    UserInformation(),
    pieChartWallet(),
  ];
  static List<Color> _colorLists = <Color>[
    Colors.blue,
    Colors.green,
    Colors.grey
  ];

  static List<Color> _colorLists2 = <Color>[
    Colors.blue.shade50,
    Colors.green.shade50,
    Colors.grey.shade100
  ];

  @override
  void initState() {
    super.initState();

    setState(() {
      checkNote(uId);
      _imageURL = FirebaseAuth.instance.currentUser!.photoURL;
    });
  }

  Future<void> uploadFile(PlatformFile file) async {
    if (kIsWeb) {
      try {
        var uploadTask = await firebase_storage.FirebaseStorage.instance
            .ref('profile/${file.name}')
            .putData(file.bytes!);
        String imageURL = await uploadTask.ref.getDownloadURL();
        setState(() {
          _imageURL = imageURL;
        });
      } on FirebaseException catch (e) {
        // e.g, e.code == 'canceled'
      }
    } else {
      try {
        var uploadTask = await firebase_storage.FirebaseStorage.instance
            .ref('profile/${file.name}')
            .putFile(File(file.path!));
        String imageURL = await uploadTask.ref.getDownloadURL();
        setState(() {
          _imageURL = imageURL;
        });
      } on FirebaseException catch (e) {
        // e.g, e.code == 'canceled'
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: _colorLists2[_pageIndex],
      appBar: AppBar(
        backgroundColor: _colorLists[_pageIndex],
        title: Text(_appBarLists[_pageIndex]),
        centerTitle: true,
        leading: Builder(
          builder: (BuildContext context) {
            return IconButton(
              icon: Icon(Icons.menu),
              onPressed: () {
                Scaffold.of(context).openDrawer();
              },
              tooltip: 'ดูข้อมูลส่วนตัว',
            );
          },
        ),
        actions: <Widget>[
          Row(
            children: [
              _imageURL == null
                  ? const Icon(Icons.person)
                  : GestureDetector(
                      child: ClipOval(
                        child: Image.network(
                          _imageURL!,
                          width: 40,
                          height: 40,
                          fit: BoxFit.fill,
                        ),
                      ),
                      // onLongPress: () async {
                      //   FilePickerResult? result =
                      //       await FilePicker.platform.pickFiles();
                      //   if (result != null) {
                      //     PlatformFile file = result.files.single;
                      //     await uploadFile(file);
                      //   } else {}
                      // },
                    )
            ],
          ),
          ElevatedButton(
            onPressed: () {
              FirebaseAuth.instance.signOut();
            },
            child: Text(
              'ออกจากระบบ',
              style: TextStyle(color: Colors.white),
            ),
            style: ElevatedButton.styleFrom(
                shadowColor: _colorLists[_pageIndex],
                primary: _colorLists[_pageIndex]),
          )
        ],
      ),
      body: _widgetLists[_pageIndex],
      drawer: DrawerWidget(
        color: _colorLists[_pageIndex],
      ),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _pageIndex,
        backgroundColor: _colorLists[_pageIndex],
        fixedColor: Colors.white,
        items: [
          BottomNavigationBarItem(
            icon: Icon(
              Icons.home,
            ),
            label: 'หน้าหลัก',
            backgroundColor: _colorLists[_pageIndex],
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.edit,
            ),
            label: 'จัดการรายการ',
            backgroundColor: _colorLists[_pageIndex],
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.pie_chart),
            label: 'สรุปรายการ',
          ),
        ],
        onTap: (index) {
          setState(() {
            _pageIndex = index;
          });
        },
      ),
    );
  }
}
