import 'dart:io';
import 'dart:js';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:file_picker/file_picker.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:firebase_storage/firebase_storage.dart' as firebase_storage;
//import 'package:flutter_authen/mainPage.dart';
import 'notePage.dart';
import 'note_service.dart';
import 'wallet_form.dart';
import 'package:flutter/cupertino.dart';
import 'dart:async';

class HomePage extends StatefulWidget {
  HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  CollectionReference users = FirebaseFirestore.instance.collection('wallet');
  String? _imageURL = null;
  @override
  void initState() {
    super.initState();
    setState(() {
      _imageURL = FirebaseAuth.instance.currentUser!.photoURL;
    });
  }

  Future<void> uploadFile(PlatformFile file) async {
    if (kIsWeb) {
      try {
        var uploadTask = await firebase_storage.FirebaseStorage.instance
            .ref('profile/${file.name}')
            .putData(file.bytes!);
        String imageURL = await uploadTask.ref.getDownloadURL();
        setState(() {
          _imageURL = imageURL;
        });
      } on FirebaseException catch (e) {
        // e.g, e.code == 'canceled'
      }
    } else {
      try {
        var uploadTask = await firebase_storage.FirebaseStorage.instance
            .ref('profile/${file.name}')
            .putFile(File(file.path!));
        String imageURL = await uploadTask.ref.getDownloadURL();
        setState(() {
          _imageURL = imageURL;
        });
      } on FirebaseException catch (e) {
        // e.g, e.code == 'canceled'
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return ListView(
      padding: EdgeInsets.all(18),
      children: [
        ListTile(
          leading: Icon(Icons.attach_money_sharp),
          title: Text('เพิ่มรายการ'),
          onTap: () async {
            Navigator.push(
                context,
                await MaterialPageRoute(
                    builder: (context) => UserForm(userId: ' ')));
          },
        ),
        ListTile(
          leading: Icon(Icons.note),
          title: Text('โน้ต'),
          onTap: () async {
            await Navigator.push(
                context, MaterialPageRoute(builder: (context) => NoteForm()));
          },
        ),
      ],
    );
  }
}
