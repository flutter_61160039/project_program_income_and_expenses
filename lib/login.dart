import 'dart:math';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

Random random = new Random();
var randomNumber = random.nextInt(3);

class LoginPage extends StatefulWidget {
  LoginPage({Key? key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  Future<UserCredential> signInWithGoogle() async {
    // Create a new provider
    GoogleAuthProvider googleProvider = GoogleAuthProvider();

    googleProvider
        .addScope('https://www.googleapis.com/auth/contacts.readonly');
    googleProvider.setCustomParameters({'login_hint': 'user@example.com'});

    return await FirebaseAuth.instance.signInWithPopup(googleProvider);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blue[50],
        appBar: AppBar(
          title: Text('เข้าสู่ระบบ'),
          centerTitle: true,
        ),
        body: Center(
          child: Container(
            padding: EdgeInsets.all(32),
            child: Column(children: [
              Text(
                'บันทึกรายรับรายจ่าย',
                style: TextStyle(fontSize: 30, color: Colors.blue),
              ),
              Container(
                margin: EdgeInsets.all(5.0),
                width: 100,
                height: 100,
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    image: DecorationImage(
                        image: AssetImage('/NEW_cb$randomNumber.gif'),
                        fit: BoxFit.fill)),
              ),
              SizedBox(
                child: Text(''),
              ),
              ElevatedButton(
                  onPressed: () async {
                    await signInWithGoogle();
                  },
                  child: Text('เข้าสู่ระบบด้วย Google')),

              //SizedBox(),
            ]),
          ),
        ));
  }
}
