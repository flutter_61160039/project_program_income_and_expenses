import 'dart:io';
import 'package:file_picker/file_picker.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_authen/wallet_Pie.dart';
import 'package:flutter_authen/wallet_information.dart';
import 'package:firebase_storage/firebase_storage.dart' as firebase_storage;
import 'wallet_form.dart';
import 'package:flutter/cupertino.dart';
import 'dart:async';


class HomePage extends StatefulWidget {
  HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  String? _imageURL = null;
  @override
  void initState() {
    super.initState();
    setState(() {
      _imageURL = FirebaseAuth.instance.currentUser!.photoURL;
    });
  }

  Future<void> uploadFile(PlatformFile file) async {
    if (kIsWeb) {
      try {
        var uploadTask = await firebase_storage.FirebaseStorage.instance
            .ref('profile/${file.name}')
            .putData(file.bytes!);
        String imageURL = await uploadTask.ref.getDownloadURL();
        setState(() {
          _imageURL = imageURL;
        });
      } on FirebaseException catch (e) {
        // e.g, e.code == 'canceled'
      }
    } else {
      try {
        var uploadTask = await firebase_storage.FirebaseStorage.instance
            .ref('profile/${file.name}')
            .putFile(File(file.path!));
        String imageURL = await uploadTask.ref.getDownloadURL();
        setState(() {
          _imageURL = imageURL;
        });
      } on FirebaseException catch (e) {
        // e.g, e.code == 'canceled'
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('หน้าหลัก'),
        centerTitle: true,
        actions: <Widget>[
          Row(
            children: [
              _imageURL == null
                  ? const Icon(Icons.person)
                  : GestureDetector(
                      child: ClipOval(
                        child: Image.network(
                          _imageURL!,
                          width: 40,
                          height: 40,
                          fit: BoxFit.fill,
                        ),
                      ),
                      onLongPress: () async {
                        FilePickerResult? result =
                            await FilePicker.platform.pickFiles();
                        if (result != null) {
                          PlatformFile file = result.files.single;
                          await uploadFile(file);
                        } else {}
                      },
                    )
            ],
          ),
          ElevatedButton(
            onPressed: () {
              FirebaseAuth.instance.signOut();
            },
            child: Text(
              'ออกจากระบบ',
              style: TextStyle(color: Colors.white),
            ),
            style: ElevatedButton.styleFrom(),
          )
        ],
      ),
      body: ListView(
        padding: EdgeInsets.all(18),
        children: [
          ListTile(
            leading: Icon(Icons.attach_money),
            title: Text('ทำรายการ'),
            onTap: () async {
              await Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => UserForm(userId: ' ')));
            },
          ),
          ListTile(
            leading: Icon(Icons.edit),
            title: Text('จัดการรายการ'),
            onTap: () async {
              await Navigator.push(context,
                  MaterialPageRoute(builder: (context) => UserInformation()));
            },
          ),
          ListTile(
            leading: Icon(Icons.pie_chart),
            title: Text('สรุปรายการ'),
            onTap: () async {
              await Navigator.push(context,
                  MaterialPageRoute(builder: (context) => pieChartWallet()));
            },
          ),
        ],
      ),
      
    );
  }
}