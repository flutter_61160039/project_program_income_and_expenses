import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';

import 'wallet_service.dart';

class UserForm extends StatefulWidget {
  String userId;
  UserForm({Key? key, required this.userId}) : super(key: key);

  @override
  _UserFormState createState() => _UserFormState(this.userId);
}

class _UserFormState extends State<UserForm> {
  String? _imageURL = null;
  String? _nameUser = null;
  String textTime = 'กดเลือกวันที่และเวลา';
  String userId;
  String uId = FirebaseAuth.instance.currentUser!.uid;
  DateTime selectedDate = DateTime.now();
  String fullName = "1";
  String company = "1";
  String age = "0";
  CollectionReference users = FirebaseFirestore.instance.collection('wallet');
  _UserFormState(this.userId);
  // TextEditingController _fullNameController = new TextEditingController();
  // TextEditingController _companyController = new TextEditingController();
  TextEditingController _ageController = new TextEditingController();

  @override
  void initState() {
    super.initState();
    _imageURL = FirebaseAuth.instance.currentUser!.photoURL;
    _nameUser = FirebaseAuth.instance.currentUser!.displayName;
    if (this.userId.isNotEmpty) {
      users.doc(this.userId).get().then((snapshot) {
        if (snapshot.exists) {
          var data = snapshot.data() as Map<String, dynamic>;
          selectedDate = data['selectedDate'].toDate();
          textTime = this.selectedDate.toString();
          this.fullName = data['type'].toString();
          this.company = data['sort'];
          age = data['money'].toString();
          // _fullNameController.text = fullName;
          // _companyController.text = company;
          _ageController.text = age;
          setState(() {
            //print(data);
          });
        }
      });
    }
  }

  final _formkey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(this.userId.length < 5 ? 'ทำรายการ' : 'แก้ไขรายการ'),
          centerTitle: true,
          actions: <Widget>[
            Row(
              children: [
                _imageURL == null
                    ? const Icon(Icons.person)
                    : GestureDetector(
                        child: ClipOval(
                          child: Image.network(
                            _imageURL!,
                            width: 40,
                            height: 40,
                            fit: BoxFit.fill,
                          ),
                        ),
                      )
              ],
            ),
            Center(
              child: Container(
                padding: EdgeInsets.all(10),
                child: Text(
                  _nameUser.toString(),
                  style: TextStyle(color: Colors.white),
                ),
              ),
            )
          ],
        ),
        body: Column(
          children: [
            Container(
              padding: EdgeInsets.all(16.0),
              child: Form(
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  key: _formkey,
                  child: Column(
                    children: [
                      ListTile(
                        leading: Text(
                          'วันที่',
                          style: TextStyle(fontSize: 20),
                        ),
                        title: TextButton(
                          onPressed: () {
                            DatePicker.showDateTimePicker(
                              context,
                              showTitleActions: true,
                              onChanged: (date) {
                                print('change $date in time zone ' +
                                    date.timeZoneOffset.inHours.toString());
                              },
                              onConfirm: (date) {
                                setState(() {
                                  selectedDate = date;
                                  textTime = selectedDate.toString();
                                  print('confirm $textTime');
                                });
                              },
                              currentTime: selectedDate,
                              locale: LocaleType.en,
                            );
                          },
                          child: Text(
                            textTime,
                            style: TextStyle(color: Colors.blue, fontSize: 20),
                          ),
                        ),
                      ),
                      ListTile(
                        title: Text('รูปแบบ', style: TextStyle(fontSize: 20)),
                      ),
                      ListTile(
                        title: Text("รายรับ"),
                        leading: Radio(
                          value: 1,
                          groupValue: int.parse(fullName),
                          onChanged: (value) {
                            setState(() {
                              fullName = value.toString();
                            });
                          },
                          activeColor: Colors.blue,
                        ),
                      ),
                      ListTile(
                        title: Text("รายจ่าย"),
                        leading: Radio(
                          value: 2,
                          groupValue: int.parse(fullName),
                          onChanged: (value) {
                            setState(() {
                              fullName = value.toString();
                            });
                          },
                          activeColor: Colors.blue,
                        ),
                      ),
                      ListTile(
                        title: Text('หมวดหมู่', style: TextStyle(fontSize: 20)),
                      ),
                      DropdownButtonFormField(
                        value: this.company,
                        items: [
                          DropdownMenuItem(
                            child: Row(
                              children: [
                                Text('เงินเดือน'),
                                SizedBox(width: 8.0)
                              ],
                            ),
                            value: '1',
                          ),
                          DropdownMenuItem(
                            child: Row(
                              children: [Text('ค่าข้าว'), SizedBox(width: 8.0)],
                            ),
                            value: '2',
                          ),
                          DropdownMenuItem(
                            child: Row(
                              children: [Text('ค่าน้ำ'), SizedBox(width: 8.0)],
                            ),
                            value: '3',
                          ),
                          DropdownMenuItem(
                            child: Row(
                              children: [Text('ค่าไฟ'), SizedBox(width: 8.0)],
                            ),
                            value: '4',
                          ),
                        ],
                        onChanged: (String? newValue) {
                          setState(() {
                            company = newValue!;
                          });
                        },
                      ),
                      ListTile(
                        title:
                            Text('จำนวนเงิน', style: TextStyle(fontSize: 20)),
                      ),
                      TextFormField(
                        controller: _ageController,
                        decoration: InputDecoration(labelText: 'จำนวน(บาท)'),
                        onChanged: (value) {
                          setState(() {
                            age = value;
                          });
                        },
                        validator: (value) {
                          if (value == null ||
                              value.isEmpty ||
                              int.tryParse(value) == null ||
                              int.tryParse(value)! < 0) {
                            return 'กรุณา ใส่เงิน';
                          }
                          return null;
                        },
                      ),
                      SizedBox(
                        child: Text(''),
                      ),
                      ElevatedButton(
                        onPressed: () async {
                          if (_formkey.currentState!.validate()) {
                            if (userId.length < 5) {
                              await addUser(
                                  uId, selectedDate, fullName, company, age);
                            } else {
                              await updateUser(userId, uId, selectedDate,
                                  fullName, company, age);
                            }

                            Navigator.pop(context);
                          }
                        },
                        child: Text('บันทึก'),
                        style: ElevatedButton.styleFrom(
                            padding: EdgeInsets.all(16)),
                      )
                    ],
                  )),
            ),
          ],
        ));
  }
}
