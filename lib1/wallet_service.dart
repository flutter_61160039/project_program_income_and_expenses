import 'package:cloud_firestore/cloud_firestore.dart';

CollectionReference users = FirebaseFirestore.instance.collection('wallet');
Future<void> delUser(userId) {
    return users
        .doc(userId)
        .delete()
        .then((value) => print('User Delete'))
        .catchError((error) => print('Failed to delete user: $error'));
  }

  Future<void> addUser(uId,selectedDate,fullName,company,age) {
    return users
        .add({
          'uId': uId,
          'selectedDate': selectedDate,
          'type': fullName,
          'sort': company,
          'money': int.parse(age)
        })
        .then((value) => print('User Added'))
        .catchError((error) => print('Failed to add user: $error'));
  }

  Future<void> updateUser(userId,uId,selectedDate,fullName,company,age) {
    return users
        .doc(userId)
        .update({
          'uId': uId,
          'selectedDate': selectedDate,
          'type': fullName,
          'sort': company,
          'money': int.parse(age)
        })
        .then((value) => print('User Updateed'))
        .catchError((error) => print('Failed to update user: $error'));
  }