import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_authen/wallet_form.dart';
import 'package:flutter_authen/wallet_Pie.dart';

import 'wallet_service.dart';

class UserInformation extends StatefulWidget {
  UserInformation({Key? key}) : super(key: key);

  @override
  _UserInformationState createState() => _UserInformationState();
}

class _UserInformationState extends State<UserInformation> {
  final Stream<QuerySnapshot> _userStream = FirebaseFirestore.instance
      .collection('wallet')
      .where('uId', isEqualTo: FirebaseAuth.instance.currentUser!.uid)
      //.orderBy('selectedDate' ,descending: true)
      .snapshots();

  Future<void> _showDialog(id) {
    Widget cancelButton = TextButton(
      child: new Text("ยกเลิก"),
      onPressed: () {
        Navigator.of(context).pop();
      },
    );
    Widget continueButton = TextButton(
      child: Text(
        "ตกลง",
        style: TextStyle(color: Colors.red),
      ),
      onPressed: () {
        Navigator.of(context).pop();
        setState(() {
          delUser(id);
        });
      },
    );
    return showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text("ลบรายการ"),
          content: Text("คุณต้องการลบรายการ ?"),
          actions: <Widget>[continueButton, cancelButton],
        );
      },
    );
  }

  void tapUpdate(id) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => UserForm(
                  userId: id,
                )));
  }

  List<DataRow> _createRows(AsyncSnapshot<QuerySnapshot> snapshot) {
    List<DataRow> newList =
        snapshot.data!.docs.map((DocumentSnapshot document) {
      Map<dynamic, dynamic> data = document.data()! as Map<String, dynamic>;
      switch (data['sort'].toString()) {
        case '1':
          sort = 'เงินเดือน';
          break;
        case '2':
          sort = 'ค่าข้าว';
          break;
        case '3':
          sort = 'ค่าน้ำ';
          break;
        case '4':
          sort = 'ค่าไฟ';
          break;
        default:
          'เงินเดือน';
      }
      return DataRow(
        cells: <DataCell>[
          DataCell(Text(data['selectedDate'].toDate().toString())),
          DataCell(Text(sort)),
          DataCell(Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Text(
                  data['type'] == '1' ? '+' : '-',
                  style: TextStyle(
                      color: Colors.black),
                ),
                Text(
                  data['money'].toString(),
                  style: TextStyle(
                      color: data['type'] == '2' ? Colors.red : Colors.green),
                )
              ],
            ),
          )),
          DataCell(Container(
            child: Row(
              children: [
                IconButton(
                  icon: Icon(Icons.edit),
                  onPressed: () {
                    tapUpdate(document.id); //
                  },
                ),
                IconButton(
                  icon: Icon(Icons.delete),
                  onPressed: () async {
                    await _showDialog(document.id); //
                  },
                ),
              ],
            ),
          )),
        ],
      );
    }).toList();
    return newList;
  }

  String sort = '';
  String? _imageURL = null;
  String? _nameUser = null;
  @override
  void initState() {
    super.initState();
    setState(() {
      _imageURL = FirebaseAuth.instance.currentUser!.photoURL;
       _nameUser = FirebaseAuth.instance.currentUser!.displayName;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('จัดการรายการ'),
          centerTitle: true,
          actions: <Widget>[
            Row(
              children: [
                _imageURL == null
                    ? const Icon(Icons.person)
                    : GestureDetector(
                        child: ClipOval(
                          child: Image.network(
                            _imageURL!,
                            width: 40,
                            height: 40,
                            fit: BoxFit.fill,
                          ),
                        ),
                      )
              ],
            ),
            Center(
            child: Container(
              padding: EdgeInsets.all(10),
              child: Text(
              _nameUser.toString(),
              style: TextStyle(color: Colors.white),
            ),)
            ,
          )
          ],
        ),
        body: Column(children: [
          Container(
            width: double.infinity,
            child: StreamBuilder(
                stream: _userStream,
                builder: (BuildContext context,
                    AsyncSnapshot<QuerySnapshot> snapshot) {
                  if (snapshot.hasError) {
                    return Text('Something went wrong');
                  }
                  if (snapshot.connectionState == ConnectionState.waiting) {
                    return Text('Loading..');
                  }
                  return DataTable(columns: <DataColumn>[
                    DataColumn(label: Text('เวลา')),
                    DataColumn(label: Text('หมวดหมู่')),
                    DataColumn(
                        label: Container(
                      child: Row(
                        children: [Text('รายรับ/'), Text('รายจ่าย')],
                      ),
                    )),
                    DataColumn(label: Text('')),
                  ], rows: _createRows(snapshot));
                }),
          ),
        ]));
  }
}
